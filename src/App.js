import React from "react";

class App extends React.Component {
  constructor(props) {
    super(props);
    //Initialization of State
    this.state = {
      buttonName: "Erase",
      checked: "Thin"
    };
  }

  componentDidMount() {
    //When Render of Page is Done we are initializing the mPad Variable and store it in state
    let mPad = new window.SignaturePad(
      document.getElementById("signature-pad"),
      {
        maxWidth: 2, //Default Brush Size
        penColor: "black", //Default Brush Color
        backgroundColor: "rgb(255, 255, 255)" //Default Background Color
      }
    );
    //Saving the pad data in State
    this.setState({
      signaturePad: mPad
    });
  }
  //on Undo Button Clicked
  onUndoClick = event => {
    //Fetching the Data Array
    let data = this.state.signaturePad.toData();
    if (data) {
      data.pop(); //Removing array item from the Stack (LIFO Pattern)
      this.state.signaturePad.fromData(data); //Passing the Rest of Data to the Signature Pad Object, it will reset the rest of data
    }
  };

  //When User wants to erase the items
  onEraseClick = event => {
    //Used Ternairy operators for the conditions
    this.state.buttonName === "Erase"
      ? this.setState(
          {
            buttonName: "Draw",
            defaultBrushSize: this.state.signaturePad.maxWidth
          },
          () => {
            this.state.signaturePad.maxWidth = 12;
            this.state.signaturePad.penColor = "white";
          }
        )
      : this.setState(
          {
            buttonName: "Erase"
          },
          () => {
            this.state.signaturePad.maxWidth = this.state.defaultBrushSize;
            this.state.signaturePad.penColor = "black";
          }
        );
  };
  onClearClick = event => {
    //Clearing the Pad on clicking on Clear Button
    this.state.signaturePad.clear();
  };
  onRadioButtonHandler = e => {
    //Handling the Radio buttons
    this.onClearClick();
    //If Condition true then changing the Size of Brush which are 2,4,6 depends on the selection.
    e.target.checked &&
      this.setState(
        {
          checked: e.target.value
        },
        () => {
          this.state.signaturePad.maxWidth =
            this.state.checked === "Thin"
              ? "2"
              : this.state.checked === "Thick"
              ? "4"
              : "6";
        }
      );
  };

  render() {
    return (
      <div>
        <canvas
          id="signature-pad"
          className="signature-pad"
          width={600}
          height={250}
        />
        <br />
        <span id="alert-text">
          {this.state.buttonName === "Draw" &&
            "Erase Mode Enabled !!! Please Click on Draw Button to Resume Drawing"}
        </span>
        <br />
        <span>Brush Thickness</span>
        <br />
        <br />
        <label>
          <input
            type="radio"
            name="brushSize"
            value="Thin"
            checked={this.state.checked === "Thin"}
            onChange={this.onRadioButtonHandler}
            defaultChecked
          />
          Thin
        </label>
        <br />
        <label>
          <input
            type="radio"
            name="brushSize"
            value="Thick"
            checked={this.state.checked === "Thick"}
            onChange={this.onRadioButtonHandler}
          />
          Thick
        </label>

        <br />
        <label>
          <input
            type="radio"
            name="brushSize"
            value="Large"
            checked={this.state.checked === "Large"}
            onChange={this.onRadioButtonHandler}
          />
          Large
        </label>
        <br />
        <div className="btn-div">
          <button id="undo" onClick={this.onUndoClick}>
            Undo
          </button>
          <button id="clear" onClick={this.onClearClick}>
            Clear
          </button>
          <button id="erase" onClick={this.onEraseClick}>
            {this.state.buttonName}
          </button>
        </div>
      </div>
    );
  }
}

export default App;
